﻿using System;
using System.Windows;
using Prism;

namespace HomeLibrarian
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        [STAThread]
        public static void Main(string[] args)
        {
            var b = new Bootsrapper();
            b.Run();
        }
    }
}