﻿using System.Windows;
using CommonServiceLocator;
using HomeLibrarian.Views;
using Prism.Ioc;
using Prism.Unity;

namespace HomeLibrarian
{
    public class Bootsrapper:PrismApplication
    {
        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            
        }

        protected override void InitializeShell(Window shell)
        {
            Current.MainWindow = shell;
            Current.MainWindow.Show();
        }

        protected override Window CreateShell()
        {
            return ServiceLocator.Current.GetInstance<Shell>();
        }
    }
}