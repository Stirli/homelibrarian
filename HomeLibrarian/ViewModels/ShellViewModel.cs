﻿using HomeLibrarian.ViewModels.Base;

namespace HomeLibrarian.ViewModels
{
    public class ShellViewModel:BaseViewModel
    {
        public ShellViewModel()
        {
            Title = "Home librarian";
        }

        public string Title { get; set; }
    }
}